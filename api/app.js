const express = require('express')
const bodyParser = require('body-parser') // convertir a objecto de javascript

const app = express()

// cargar rutas
let user_routes = require('./routes/user')

//middlewares

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())


// cors 


//rutas
app.use('/api', user_routes);


// exportar
module.exports = app;