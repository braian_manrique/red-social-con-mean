const mongoose = require('mongoose')

const Schema = mongoose.Schema;

let MessageSchema = Schema({
    text: String,
    create_at: String,
    emmiter: { type: ObjectId, ref: 'User' },
    receiver: { type: ObjectId, ref: 'User' }
})

module.exports = mongoose.model('Message', MessageSchema)