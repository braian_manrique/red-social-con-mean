const mongoose = require('mongoose')

const Schema = mongoose.Schema;


// user es el usuario que sigue y followed es el usuario seguido
let FollowSchema = Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    followed: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Follow', FollowSchema)