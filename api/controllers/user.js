const User = require('../models/user');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('../services/jwt');
const mongoosePaginate = require('mongoose-pagination')


// Register
function saveUser(req, res) {
    let user = new User();
    let params = req.body;

    if (params.name && params.surname && params.nick && params.email && params.password) {
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;

        // Verificar si ya existe el usuario registrado en la base de datos
        User.find({
            $or: [
                { email: user.email.toLowerCase() },
                { nick: user.nick.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion de usuarios' })

            if (users && users.length >= 1) {
                return res.status(200).send({ message: 'El usuario ya se encuenta registrado.' })
            } else {

                // Cifra los datos y los guarda
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;

                    user.save((err, userStored) => {
                        if (err) return res.status(500).send({ message: 'Error al guardar el usuario' });

                        if (userStored) {
                            res.status(200).send({ user: userStored })
                        } else {
                            res.status(4040).send({ message: 'No se ha registrado el usuario' })
                        }

                    });

                })
            }
        });

    } else {
        res.status(200).send({
            message: 'Ingresa todos los campos!!!'
        })
    }

}

//Ingresar o loguearse
function loginUser(req, res) {
    let params = req.body;
    let email = params.email;

    let password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if (err) return res.status(500).send({ message: 'ERROR en la peticion' });

        if (user) {
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {
                    //devolver datos de usuario
                    if (params.gettoken) {
                        // generar y devolver  token
                        return res.status(200).send({
                                token: jwt.createToken(user)
                            })
                            // generar token

                    } else {
                        // devolver datos del usuario en claro
                        user.password = undefined;
                        return res.status(200).send({ userLogeado: user })
                    }

                } else {
                    return res.status(404).send({ message: 'El usuario no se ha podido identificar' });
                }
            });
        } else {
            return res.status(404).send({ message: 'El usuario no se ha podido identificar !!!!' });
        }
    });
}


function getUser(req, res) {
    let userId = req.params.id; // se usa params cuando llegan datos por la url

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(500).send({
                message: 'Error en la peticion'
            })
        }
        if (!user) {
            return res.status(404).send({
                message: 'El usuario no existe!!'
            })
        }

        return res.status(200).send({ user })
    })

}

// Devolver listado de usuarios paginado
function getUsers(req, res) {
    let identity_user_id = req.user.sub;
    let page = 1;

    if (req.params.page) {
        page = req.params.page
    }
    
    let itemsPerPage = 2;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' })

        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' })

        return res.status(200).send({
            usuarios_Actuales: users,
            total_Usuarios: total,
            pages: Math.ceil(total / itemsPerPage)

        })
    })
}

//Permite actualizar los datos de un Usuario

function updateUser(req, res) {

    let userId = req.params.id;

    let update = req.body;

    // Borrar la propiedad password
    delete update.password
    
    if(userId != req.user.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar los datos del usuario'})
    }

    User.findByIdAndUpdate(userId, update, { new: true} ,(err, userUpdated) =>{
        if (err) return res.status(500).send({ message: 'Error en la peticion' })  

        if(!userUpdated) return res.status(404).send({ message: 'No se pudo actualizar el usuario'})

        return res.status(200).send({
            user: userUpdated
        })
    })

}

module.exports = {
    saveUser,
    loginUser,
    getUser,
    getUsers,
    updateUser
}