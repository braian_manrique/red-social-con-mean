const mongoose = require('mongoose');
const app = require('./app')
const port = 3800;

// Conexión a la bd
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/mean_social', { useNewUrlParser: true })
    .then(() => {
        console.log('La conexion a la base de datos se ha realizado correctamente.!! ');

        // crear servidor
        app.listen(port, () => {
            console.log('Servidor corriendo ok');
        })
    })
    .catch(err => {
        console.log(err);
    })